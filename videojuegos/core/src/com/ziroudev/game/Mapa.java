package com.ziroudev.game;

public class Mapa {
	
	int dimX = 10, dimY = 10;
	Object[][] tablero;
	
	public void creaMapa(){
		tablero = new Object[dimX][dimY];
		for(int i=0; i<dimX; i++){
			for(int j=0; j<dimY; j++){
				tablero[i][j] = 0;
			}
		}
	}
	
	public void muestraMapa(){
		for(int i=0; i<dimX; i++){
			for(int j=0; j<dimY; j++){
				System.out.print(tablero[i][j]);
			}
			System.out.println("");
		}
	}
}
